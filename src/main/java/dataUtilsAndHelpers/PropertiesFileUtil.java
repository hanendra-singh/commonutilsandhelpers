package dataUtilsAndHelpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.Set;
import extraUtilsAndHelpers.*;

/* This class will have common methods related to working with Properties files*/
public class PropertiesFileUtil {

	private static Properties props = null;
	
	
	/* Load properties file  */	
	public static Properties loadProperties(String propFilePath){
		try{
			InputStream ins = new FileInputStream(new File(propFilePath));
			if (props == null) {
				props = new Properties();
				props.load(ins);
			}else{
				Properties properties = new Properties();
				properties.load(ins);
				props.putAll(properties);
				properties = null;
			}
			//LogHelper.info("Loaded Properties file");
		}catch (Exception e){
			LogHelper.error("Unable to load Properties file", e);
			e.printStackTrace();
		}
		return props;
	}

	/**
	 * Look up a property from the properties file.
	 *
	 * @param key
	 *            The name of the property to be found
	 * @return The value of the property
	 */
	public static String getProperty(String key) {
		String value = null;
		if (props == null) {
			LogHelper.error("Properties file was not loaded" , new Exception());
			return "";
		}
		try {
			value = props.getProperty(key);
			LogHelper.info("Property key: " + key + "   ||    Property Value: "+value);
		} 
		catch (Exception IOException){
			LogHelper.error("Unable to find key: " + key, IOException);
		}
		return value;
	}

	/**
	 * Look up for Map of key values associated with given keyword from the properties file.
	 *
	 * @param keyword
	 *            The name of the property contains keyword to be found
	 * @return The list of values associated with given property keyword
	 */
	/*public static HashMap<String,String> getProperties(String keyword) {
		HashMap<String,String> hashMap = new HashMap<String, String>();
		if (props == null) {
			LogHelper.error("Properties file was loaded" , new Exception());
			return hashMap;
		}
		try {
			Set<String> keys = props.stringPropertyNames();        	
			//LogHelper.info("Method : getProperties() - Populating HashMap with below Key and values");

			for(String key : keys) {
				if(key != null && key.contains(keyword)) { 
					String newKey = key.replace(keyword, "");
					hashMap.put(newKey,props.getProperty(key));
					//LogHelper.info("Property key: " + key + "   ||   Property Value: "+props.getProperty(key));
				}   
			}
		} catch (Exception IOException){
			LogHelper.error("Error while extracting Map from properties file on basis of keyword: ", IOException);
		}
		return hashMap;
	} */

	// Set multiple properties to the given file
	@SuppressWarnings("deprecation")
	public static void SetProperties(LinkedHashMap<String,String> values, String filename) {

		Properties prop= new Properties();
		prop.putAll(values);
		File file= new File(filename);
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(file);
			prop.save(fileOut, "saving Values to file");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 

	// Set single property to the given file
	@SuppressWarnings("deprecation")
	public static void SetProperty(String key, String value, String filename) {

		try {
			InputStream fileinput = new FileInputStream(filename);
			File file= new File(filename);
			Properties prop= new Properties();
			prop.load(fileinput);
			prop.setProperty(key, value);
			FileOutputStream fileOut = new FileOutputStream(file);
			prop.store(fileOut, "Updating the data in HMS template file");
			fileOut.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 

} 